/*
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
var fps           = 25;
var max_frames    = 30 * fps; // 30 seconds
//
// 4K UHD resolution
var canvas_w      = 3840;
var canvas_h      = 2160;

function next() {
    set_background_color(new color(current_frame / max_frames, 0, 0, 1));
}
