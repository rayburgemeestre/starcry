.. Starcry documentation master file, created by
   sphinx-quickstart on Tue Jul 19 20:39:27 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Starcry's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2
   :caption: Getting started

   getting_started
   cli_usage

.. toctree::
   :maxdepth: 2
   :caption: Javascript API

   javascript_api

.. toctree::
   :maxdepth: 2
   :caption: Web UI

   web_ui

.. toctree::
   :maxdepth: 2
   :caption: Developer Manual

   developer_manual

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

